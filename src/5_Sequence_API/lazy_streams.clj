; Prime number generator using an infinite lazy sequence.

(defn prime-generator
  [lst]
  (lazy-seq (cons (first lst)
                  (prime-generator
                    (remove #(zero? (rem % (first lst)))
                            (rest lst))))))

(def prime (prime-generator (iterate inc 2)))
