; More examples using core.logic.

(use '[clojure.core.logic :rename {== ===}])

(defn my-last
  [lst]
  (cond
    (= () (rest lst))     (first lst)
    (not= () (rest lst))  (my-last (rest lst))))

(defn lasto
  [lst result]
  (conde
    [(fresh [tail]
       (conso result tail lst)
       (=== tail []))]
    [(fresh [_ tail]
       (conso _ tail lst)
       (!= tail [])
       (lasto tail result))]))

(defn my-reverse
  [lst]
  (cond
    (= () lst)    ()
    (!= () lst)   (concat (my-reverse (rest lst)) [(first lst)])))

(defn reverseo
  [lst result]
  (conde
    [(=== lst [])
     (=== result [])]
    [(!= lst [])
     (fresh [head tail new-result]
       (conso head tail lst)
       (reverseo tail new-result)
       (appendo new-result [head] result))]))