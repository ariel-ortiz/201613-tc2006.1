; Examples using sorting operations.

(def students
     [{ :id 123
        :name "John"
        :grade 89.1}
      { :id 299
        :name "Mary"
        :grade 99.5}
      { :id 666
        :name "Damien"
        :grade 66.6}
      { :id 199
        :name "Lucy"
        :grade 99.5}])

(defn compare-students
  [s1 s2]
  (if (= (:grade s1) (:grade s2))
    (compare (:name s1) (:name s2))
    (compare (:grade s1) (:grade s2))))

(defn qsort
  [lst]
  (if (empty? lst)
    ()
    (let [pivot (first lst)
          rest-list (rest lst)
          less-list (filter #(< % pivot) rest-list)
          greater-list (filter #(>= % pivot) rest-list)]
      (concat
        (qsort less-list)
        (list pivot)
        (qsort greater-list)))))



