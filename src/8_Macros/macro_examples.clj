; Examples using macros.

(defmacro example1
  [a b c]
  (list 'quote (list a b c)))

(defmacro example2
  []
  (list '+ 1 2))

(defmacro my-and
  ([] true)
  ([a] a)
  ([a & b]
   `(let [t# ~a]
      (if t#
        (my-and ~@b)
        t#))))

(defmacro debug
  [expr]
  `(let [t# ~expr]
     (do (println "Debug:" '~expr "=>" t#)
         t#)))

; An example on how to use the debug macro defined above.
(defn !
  [n]
  (debug n)
  (if (zero? n)
    1
    (debug (* n (! (dec n))))))

(defn produce-body
  [params exprs]
  (if (empty? params)
    `(do ~@exprs)
    `(fn [~(first params)]
       ~(produce-body (rest params) exprs))))

(defmacro defn-curry
  [name args & exprs]
  (if (zero? (count args))
    `(defn ~name [] (do ~@exprs))
    `(defn ~name [~(first args)]
       ~(produce-body (rest args) exprs))))