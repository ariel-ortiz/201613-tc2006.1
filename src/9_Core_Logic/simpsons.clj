; First example using core.logic

(use '[clojure.core.logic :rename {== ===}])
(use 'clojure.core.logic.pldb)

(db-rel maleo person)
(db-rel femaleo person)
(db-rel parento parent child)

(def simpsons
  (db
    [maleo :homer]
    [maleo :bart]
    [maleo :abe]
    [maleo :clansy]
    [femaleo :marge]
    [femaleo :lisa]
    [femaleo :maggie]
    [femaleo :mona]
    [femaleo :selma]
    [femaleo :patty]
    [femaleo :jackie]
    [parento :homer :bart]
    [parento :marge :bart]
    [parento :homer :lisa]
    [parento :marge :lisa]
    [parento :homer :maggie]
    [parento :marge :maggie]
    [parento :mona :homer]
    [parento :abe :homer]
    [parento :jackie :marge]
    [parento :clansy :marge]))

(defn mothero
  [mom person]
  (all
    (femaleo mom)
    (parento mom person)))

(defn fathero
  [dad person]
  (all
    (maleo dad)
    (parento dad person)))

(defn sistero
  [sibling person]
  (fresh [mom dad]
    (!= sibling person)
    (femaleo sibling)
    (mothero mom sibling)
    (mothero mom person)
    (fathero dad sibling)
    (fathero dad person)))

(defn brothero
  [sibling person]
  (fresh [mom dad]
    (!= sibling person)
    (maleo sibling)
    (mothero mom sibling)
    (mothero mom person)
    (fathero dad sibling)
    (fathero dad person)))

;(defn grand-fathero
;  [grandad person]
;  (conde
;    [(fresh [dad]
;       (fathero grandad dad)
;       (fathero dad person))]
;    [(fresh [mom]
;       (fathero grandad mom)
;       (mothero mom person))]))

(defn grand-fathero
  [grandad person]
  (fresh [x]
    (fathero grandad x)
    (parento x person)))

;--------------------------------------------------------------------

; Who is a female?
(run-db* simpsons [q] (femaleo q))

; Is Marge a female?
(run-db* simpsons [q] (femaleo :marge))

; Who is Lisa's mother?
(run-db* simpsons [q] (mothero q :lisa))

; Who are Marge's children?
(run-db* simpsons [q] (mothero :marge q))

; Give me the pairs of sister relations.
(run-db* simpsons [q1 q2] (sistero q1 q2))

; Give me the pairs of grand-father relations.
(run-db* simpsons [q1 q2] (grand-fathero q1 q2))