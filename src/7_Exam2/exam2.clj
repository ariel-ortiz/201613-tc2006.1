;==========================================================
; Type your name and student ID here.
;==========================================================

(use 'clojure.test)
(use '[clojure.string :only (ends-with?)])

;==========================================================
(defn pow
  "Returns a raised to the power b."
  [a b]
  (reduce * (repeat b a)))

;==========================================================
(defn lcm
  "Returns the least common multiple of the two
  positive non-zero integers x and y."
  [x y]
  (->>
    (range x (inc (* x y)) x)
    (drop-while #(not= 0 (rem % y)))
    first))

;==========================================================

(defn get-divisor
  [d]
  (->>
    (iterate #(* % 10) 10)
    (drop-while #(<= % d))
    first))

;(defn last-digits-of-divisors
;  "Returns the number of divisors of n that end
;  with the same digits as d."
;  [n d]
;  (->>
;    (range 1 (inc n))
;    (filter #(zero? (rem n %)))
;    (filter #(ends-with? (str %) (str d)))
;    count))

(defn last-digits-of-divisors
  "Returns the number of divisors of n that end
  with the same digits as d."
  [n d]
  (->>
    (range 1 (inc n))
    (filter #(zero? (rem n %)))
    (filter #(= d (rem % (get-divisor d))))
    count))

;==========================================================
(deftest test-pow
  (is (= 1 (pow 0 0)))
  (is (= 0 (pow 0 1)))
  (is (= 1 (pow 5 0)))
  (is (= 5 (pow 5 1)))
  (is (= 125 (pow 5 3)))
  (is (= 25 (pow -5 2)))
  (is (= -125 (pow -5 3)))
  (is (= 1024 (pow 2 10)))
  (is (= 525.21875 (pow 3.5 5)))
  (is (= 129746337890625 (pow 15 12)))
  (is (= 3909821048582988049 (pow 7 22))))

;==========================================================
(deftest test-last-digits-of-divisors
  (is (= 1 (last-digits-of-divisors 1 1)))
  (is (= 3 (last-digits-of-divisors 84 4)))
  (is (= 2 (last-digits-of-divisors 100 5)))
  (is (= 12 (last-digits-of-divisors 1600 0)))
  (is (= 3 (last-digits-of-divisors 2894067 23)))
  (is (= 3 (last-digits-of-divisors 25000 250)))
  (is (= 2 (last-digits-of-divisors 13862756 1234)))
  (is (= 0 (last-digits-of-divisors 1234567 99999))))

;==========================================================
(deftest test-lcm
  (is (= 12 (lcm 4 6)))
  (is (= 80 (lcm 16 20)))
  (is (= 15 (lcm 15 15)))
  (is (= 20 (lcm 10 20)))
  (is (= 42 (lcm 21 6)))
  (is (= 561741 (lcm 123 4567)))
  (is (= 98084900347 (lcm 412619 237713))))

;==========================================================
(run-tests)
