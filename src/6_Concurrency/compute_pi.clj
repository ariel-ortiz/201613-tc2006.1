; Computing the value of Pi.
(defn compute-pi-slice
  [num-rects start end]
  (let [width (/ 1.0 num-rects)]
    (* (loop [i   start
              sum 0.0]
         (if (= i end)
           sum
           (let [mid (* (+ i 0.5) width)
                 height (/ 4.0 (+ 1.0 (* mid mid)))]
             (recur (inc i) (+ sum height)))))
       width)))

(defn split
  [num-rects n]
  (let [size (/ num-rects n)]
    (->>
      (range 0 num-rects size)
      (map #(list % (+ % size))))))

(defn compute-pi
  [num-rects n]
  (->>
    (split num-rects n)
    (pmap (fn [[start end]]
            (compute-pi-slice num-rects start end)))
    (reduce +)))
