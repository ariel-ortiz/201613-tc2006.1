; Examples of Higher-Order functions

; Takes a function as a parameter.
(defn my-map
  [f lst]
  (if (empty? lst)
    ()
    (cons (f (first lst))
          (my-map f (rest lst)))))

; Returns a function as its result.
(defn add
  [x]
  (fn [y] (+ x y)))