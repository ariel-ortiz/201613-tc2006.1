; A few examples using recursion

(use 'clojure.test)

(defn !
  [n]
  (if (zero? n)
    1N
    (* n (! (dec n)))))

(defn !!
  [n]
  (loop [val n
         x 1N]
    (cond
      (zero? val) x
      :else (recur (dec val) (* x val)))))

(defn pow
  [m n]
  (if (zero? n)
    1
    (* m (pow m (dec n)))))

(defn pow2
  [m n]
  (loop [i n
         r 1]
    (if (zero? i)
      r
      (recur (dec i) (* r m)))))


(defn dup
  [lst]
  (if (empty? lst)
    ()
    (cons (first lst)
          (cons (first lst)
                (dup (rest lst))))))

(defn largest
  [lst]
  (if (= 1 (count lst))
    (first lst)
    (let [x (largest (rest lst))]
      (if (> x (first lst))
        x
        (first lst)))))

(defn largest2
  [lst]
  (loop [biggest (first lst)
         lst (next lst)]
    (if lst
      (recur (if (> (first lst) biggest)
               (first lst)
               biggest)
             (next lst))
      biggest)))

(defn how-many
  [x lst]
  (cond
    (empty? lst)        0
    (list? (first lst)) (+ (how-many x (first lst))
                           (how-many x (rest lst)))
    (= x (first lst))   (inc (how-many x (rest lst)))
    :else               (how-many x (rest lst))))

(defn incall
  [lst]
  (if (empty? lst)
    lst
    (cons (inc (first lst))
          (incall (rest lst)))))

(defn incall2
  [lst]
  (loop [a lst
         b ()]
    (if (empty? a)
      (reverse b)
      (recur (rest a)
             (cons (inc (first a)) b)))))

(deftest test-!
  (is (= 1 (! 0)))
  (is (= 1 (! 1)))
  (is (= 120 (! 5)))
  (is (= 3628800N (! 10))))

(deftest test-pow
  (is (= 1024 (pow 2 10)))
  (is (= 1 (pow 23 0)))
  (is (= 7 (pow 7 1)))
  (is (= 125 (pow 5 3))))

(deftest test-dup
  (is (= () (dup ())))
  (is (= '(1 1)
         (dup '(1))))
  (is (= '(1 1 4 4 7 7 8 8 0 0)
         (dup '(1 4 7 8 0)))))

(deftest test-largest
  (is (= 3 (largest '(3))))
  (is (= -1 (largest '(-1 -2 -3 -4))))
  (is (= 10 (largest '(5 -1 10 5 7 4 -20 4)))))

(deftest test-how-many
  (is (= 3 (how-many 2 '(1 3 2 5 2 2 1))))
  (is (= 0 (how-many 7 '(1 3 2 5 2 2 1))))
  (is (= 5) (how-many 3 '(3 (2 (1 3 3) ((3 2 3))) 1))))

(run-tests)