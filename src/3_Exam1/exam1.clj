;==========================================================
; First practical exam solution.
;==========================================================

(use 'clojure.test)

;==========================================================

(defn !
  [n]
  (if (zero? n)
    1
    (* n (! (dec n)))))

(defn binomial
  [k i]
  (/ (! k) (* (! i) (! (- k i)))))

(defn bernoulli
  "Returns the bernoulli number Bk."
  [k]
  (if (zero? k)
    1
    (loop [sum 0
           i   0]
      (if (= k i)
        (- sum)
        (recur
          (+ sum (* (binomial k i)
                    (/ (bernoulli i)
                       (- (inc k) i))))
          (inc i))))))

;==========================================================
(defn sum-accum-list
  "Takes lst as input (a list of numbers) and returns a new
  list where each element xi of the resulting list is the
  sum of all the elements xj from the original list, where
  j <= i."
  [lst]
  (loop [lst lst
         accum ()
         sum 0]
    (if (empty? lst)
      (reverse accum)
      (let [new-value (+ sum (first lst))]
        (recur (rest lst)
               (cons new-value accum)
               new-value)))))

;==========================================================
(deftest test-bernoulli
  (is (= 1 (bernoulli 0)))
  (is (= -1/2 (bernoulli 1)))
  (is (= 1/6 (bernoulli 2)))
  (is (= 0 (bernoulli 3)))
  (is (= -1/30 (bernoulli 4)))
  (is (= 0 (bernoulli 5)))
  (is (= 1/42 (bernoulli 6)))
  (is (= 0 (bernoulli 7)))
  (is (= -1/30 (bernoulli 8)))
  (is (= 0 (bernoulli 9)))
  (is (= 5/66 (bernoulli 10)))
  (is (= -691/2730 (bernoulli 12))))

;==========================================================
(deftest test-sum-accum-list
  (is (= ()
         (sum-accum-list ())))

  (is (= '(1 3 6 10 15 21 28 36 45 55)
         (sum-accum-list '(1 2 3 4 5 6 7 8 9 10))))

  (is (= '(1 2 3 4 5)
         (sum-accum-list '(1 1 1 1 1))))

  (is (= '(4 12 27 43 66 108)
         (sum-accum-list '(4 8 15 16 23 42)))))

;==========================================================
(run-tests)
