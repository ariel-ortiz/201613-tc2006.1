; A very cpu-intensive function.
(defn fibo
  [n]
  (if (< n 2)
    n
    (+ (fibo (- n 1)) (fibo (- n 2)))))

(pmap fibo [45 45 45 45])